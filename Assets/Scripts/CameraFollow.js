﻿#pragma strict

var target : GameObject;
private var originalx: float;
private var originaly: float;
private var originalz: float;
private var height: float;

function Start () {
	originalx = target.transform.position.x;
	originaly = target.transform.position.y;
	originalz = target.transform.position.z;
}

function Update () {
	var newx = target.transform.position.x;
	var newy = target.transform.position.y;
	var newz = target.transform.position.z;
	gameObject.transform.Translate(new Vector3(newx- originalx ,newy - originaly,newz - originalz));
	originalx = newx;
	originaly = newy;
	originalz = newz;
}