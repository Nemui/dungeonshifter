﻿using UnityEngine;
using System.Collections;

public class Hp : MonoBehaviour {
	int maxHp = 100;
	int curHp = 100;
	float barLen = 1;
	// Use this for initialization
	void Start () {
		barLen = Screen.width / 2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Damage(int dmg){
		curHp -= dmg;
		barLen = (Screen.width / 2) * (curHp / (float)maxHp);
		if (curHp < 0)
			curHp = 0;
	}
	public void Heal(int heal){
		curHp += heal;
		if (curHp > maxHp)
			curHp = maxHp;
		barLen = (Screen.width / 2) * (curHp / (float)maxHp);
	}
	void OnGUI(){
		Rect rect = new Rect (10, 10, barLen, 20);
		GUI.Box (rect, curHp + " / " + maxHp);
	}
}