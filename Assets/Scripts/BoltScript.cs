﻿using UnityEngine;
using System.Collections;

public class BoltScript : MonoBehaviour {
	private int GTAMode = 0;
	private Vector2 direction = Vector2.zero;
	public float speed = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (direction != Vector2.zero) {

			transform.Translate(new Vector3(direction.x, direction.y)*speed*Time.deltaTime, Space.World);
		}

	}



	void OnCollisionEnter2D(Collision2D c){
		GameObject go = c.gameObject;
		if (GTAMode >0) {
			GameObject.Destroy(go);
		} else {
			if(go.tag == "enemy") {
				GameObject.Destroy(go);
			}
			GameObject.Destroy(gameObject);
		}
	}

	public void SetDirection(Vector2 d){
		direction = d;
		if (d == Vector2.up)
						transform.Rotate (new Vector3 (0, 0, 90));
		else if(d == Vector2.right *-1)
			transform.Rotate ( new Vector3 (0, 0, 180));
		else if(d == Vector2.up *-1)
			transform.Rotate (new Vector3 (0, 0, 270));
	}

	void OnCollisionEnter(Collision c){
		Destroy (this.gameObject);
	}
}
