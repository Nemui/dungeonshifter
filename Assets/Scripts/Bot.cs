﻿using UnityEngine;
using System.Collections;

public class Bot : MonoBehaviour {
	public int hugDmg = 5;
	enum state {patrol, going, fight,fighting, wait}
	state myState;
	float range = 3;
	float targetRange = 3;
	float speed = 1;
	int MaxSteps =60;
	int currentStep;
	Vector2 target = new Vector2(0,0);
	Vector2 going = new Vector2(0,0);
	Form form;

	// Use this for initialization
	void Start () {
		myState = state.patrol;
		currentStep = 0;
		form = GetComponent<Form>();
	}

	public void ChangeForm(){
		form.Henge();
	}

	// Update is called once per frame
	void Update () {
		switch(myState) {
		case state.patrol:
			//Debug.Log("Patrolling");
			Patrol();
			break;
		case state.going:
			//Debug.Log("going");
			Go();
			break;
		case state.wait:
			//Debug.Log("going");
			Wait();
			break;
		case state.fight:
			//Debug.Log("going");
			Fight();
			break;
		case state.fighting:
			//Debug.Log("going");
			Fighting();
			break;
		}


	}

	void FindEnemy(){
		GameObject go = GameObject.FindGameObjectWithTag("Player");
		target = gameObject.transform.position - go.transform.position;
		//Debug.Log(target);
	}

	void Wait() {
		currentStep++;
		if(currentStep > MaxSteps) {
			currentStep = 0;
			myState = state.patrol;
		}
	}

	void Fight() {
		FindEnemy();
		if(CheckForAgro()) {
			myState = state.fighting;
			currentStep=0;
		} else {
			myState = state.patrol;
			currentStep=0;
		}


	}

	void Fighting() {
		if(CheckForAgro()) {
			if(target.x <= target.y){
				if(target.x <=0) {
					rigidbody2D.velocity= new Vector2(0,1);
					form.facing = rigidbody2D.velocity;
				} else {
					rigidbody2D.velocity= new Vector2(0,-1);
					form.facing = rigidbody2D.velocity;
				}
			} else {
				if(target.y <=0) {
					rigidbody2D.velocity= new Vector2(-1,0);
					form.facing = rigidbody2D.velocity;
				} else {
					rigidbody2D.velocity= new Vector2(1,0);
					form.facing = rigidbody2D.velocity;
				}
			}
			if(Time.time > form.spellCool + form.spellTime) {
				form.Fire();
				form.spellTime = Time.time;
				FindEnemy();
			}
		} else {
			myState = state.wait;
			currentStep = 0;
		}

	}

	bool CheckForAgro() {
		return target.magnitude < targetRange;

	}

	void Go(){

		rigidbody2D.velocity = (going * speed);
		//Debug.Log(rigidbody2D.velocity);
		currentStep++;
		if(currentStep > MaxSteps){
			myState = state.patrol;
		}
		if(CheckForAgro()) {
			myState = state.fight;
		}
	}

	void Patrol() {
		FindEnemy();
		float goingx  = (Random.value*2 -1 )*range;
		float goingy = (Random.value*2 -1)*range;
		//float currentz = gameObject.transform.position.z;
		going = new Vector2(goingx,goingy);

		myState = state.going;

		currentStep=0;
	}

	void OnCollisionEnter2D(Collision2D c){
		if (c.gameObject != null && c.gameObject.tag == "Player") {
			GameObject.Find("PlayerStats").GetComponent<Hp>().Damage(hugDmg);
		}
	}
}
