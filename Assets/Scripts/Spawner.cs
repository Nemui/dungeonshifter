﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	private int count;
	public Object enemy;
	public float timeStep;
	private float lastCreate = 0;
	// Use this for initialization
	void Start () {
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time> lastCreate+timeStep) {
			count++;
			GameObject.Instantiate(enemy, gameObject.transform.position,Quaternion.identity);
			lastCreate = Time.time;
		}
	}
	
}
