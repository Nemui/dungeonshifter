﻿using UnityEngine;
using System.Collections;

public class SwordScript : MonoBehaviour {
	public float swingSpeed = 10f;
	public Transform wielder;
	public float swingDuration = 0.2f;
	private float swingStart = 0;


	// Use this for initialization
	void OnEnable () {
		swingStart = Time.time;

	}
	
	// Update is called once per frame
	void Update () {
		transform.position = wielder.position;
		transform.Rotate (new Vector3(0,0,swingSpeed));
		if (Time.time > swingStart + swingDuration)
			this.gameObject.SetActive (false);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "enemy") {
			Destroy (other.gameObject);
		}
	}
}
