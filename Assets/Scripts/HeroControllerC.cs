﻿using UnityEngine;
using System.Collections;

public class HeroControllerC : MonoBehaviour {


	Form form;

	// Use this for initialization
	void Start () {
		form = GetComponent<Form>();
		form.anim = transform.GetComponent<Animator> ();
		form.sr = GetComponent<SpriteRenderer>();
		form.standing = form.downSprite;

	}



	// Update is called once per frame
	void Update () {
		Form form = gameObject.GetComponent<Form>();
		if (Input.GetButtonDown("Fire1") && Time.time > form.spellTime + form.spellCool) {

			form.Fire();
		}
		if (Input.GetButtonDown("Fire2") && Time.time > form.spellTime + form.spellCool) {
			form.Henge();
			GameObject[] gos = GameObject.FindGameObjectsWithTag("enemy");
			foreach(GameObject go in gos) {
				Bot script = go.GetComponent<Bot>();
				if(script != null) {
					script.ChangeForm();
				}
			}
		}
	}

	void FixedUpdate(){
		float moveH = Input.GetAxis ("Horizontal");
		float moveV = Input.GetAxis ("Vertical");
		rigidbody2D.velocity = new Vector2 (moveH * form.walkSpeed, moveV * form.walkSpeed);
		if (moveH != 0 || moveV != 0)
						form.anim.enabled = true;
				else {
						form.anim.enabled = false;
			form.sr.sprite = form.standing;
				}

		if (moveV > 0) {
			form.newFacing = Vector2.up;
			form.standing = form.upSprite;
		} else if (moveV < 0) {
			form.newFacing = Vector2.up * -1;
			form.standing = form.downSprite;
		}
		else {
			if(moveH > 0){
				form.newFacing = Vector2.right;
				form.standing = form.rightSprite;
			}else if(moveH < 0){
				form.newFacing = Vector2.right * -1;
				form.standing = form.leftSprite;
			}else{

				//sr.sprite = standing;

			}

		}
		form.facing = form.newFacing;
		form.anim.SetFloat ("Horizontal", moveH);
		form.anim.SetFloat ("Vertical", moveV);
	}
}
